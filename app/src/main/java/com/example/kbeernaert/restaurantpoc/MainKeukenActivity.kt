package com.example.kbeernaert.restaurantpoc

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.util.ArrayList

class MainKeukenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_keuken)

        var listView = findViewById<ListView>(R.id.listView)
        var datamodels: ArrayList<MenuItem> = ArrayList()
        var dataModelsClicked: ArrayList<MenuItem> = ArrayList()
        var mDatabase = FirebaseDatabase.getInstance().getReference();
        var adapter: CustomAdapter
        adapter = CustomAdapter(datamodels, getApplicationContext())
        val ordersKeukenDB = mDatabase.child("ordersKeuken")
        val buttonConfirm = findViewById<Button>(R.id.buttonConfirm)
        buttonConfirm.setAlpha(.5f);
        buttonConfirm.setEnabled(false)

        ordersKeukenDB.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (i in dataSnapshot.children) {
                    Log.d("Tag", i.toString())
                    datamodels.add(MenuItem(i.child("name").value.toString(), "keuken", false, i.key))
                }
                adapter = CustomAdapter(datamodels, getApplicationContext())
                listView.setAdapter(adapter);
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }

        })

        listView.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                Log.d("tag", "clicked")
                val dataModel = datamodels.get(position)
                var found: Boolean = false
                dataModel.checked = !dataModel.checked
                Log.d("tag", dataModel.name + ' ' + dataModel.checked + ' ' +dataModel.id)
                if(dataModel.checked) {
                    dataModelsClicked.add(dataModel)
                } else {
                    for (i in dataModelsClicked) {
                        if (i.name === dataModel.name) {
                            dataModelsClicked.remove(i)
                            break
                        }
                    }

                }
                if(dataModelsClicked.size > 0) {
                    Log.d("is not empty", dataModelsClicked.toString())
                    buttonConfirm.setAlpha(1f);
                    buttonConfirm.setEnabled(true)
                } else {
                    Log.d("is empty", dataModelsClicked.toString())
                    buttonConfirm.setAlpha(.5f);
                    buttonConfirm.setEnabled(false)
                }
                adapter.notifyDataSetChanged()

            }
        }

        buttonConfirm.setOnClickListener(View.OnClickListener {
            for( i in dataModelsClicked) {
                Log.d("debug", i.id)
                mDatabase.child("ordersKeuken").child(i.id).removeValue()
            }

            val context: Context = getApplicationContext();
            val text: CharSequence = "De orders zijn klaar voor de klant...";
            val duration = Toast.LENGTH_SHORT;

            val toast: Toast = Toast.makeText(context, text, duration);
            toast.show()

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        })
    }
}
