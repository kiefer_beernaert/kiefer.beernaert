package com.example.kbeernaert.restaurantpoc

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val buttonZaal = findViewById<Button>(R.id.button) as Button
        buttonZaal.setOnClickListener {
            Log.d("MainActivity", "Zaal clicked")
            val intent = Intent(this, MainZaalActivity::class.java)
            startActivity(intent)
        }

        val buttonKlant = findViewById<Button>(R.id.button2) as Button
        buttonKlant.setOnClickListener {
            Log.d("MainActivity", "Klant clicked")
            val intent = Intent(this, MainKlantActivity::class.java)
            startActivity(intent)
        }

        val buttonKeuken = findViewById<Button>(R.id.button3) as Button
        buttonKeuken.setOnClickListener {
            Log.d("MainActivity", "Keuken clicked")
            val intent = Intent(this, MainKeukenActivity::class.java)
            startActivity(intent)
        }
    }
}
