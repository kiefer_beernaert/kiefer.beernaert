package com.example.kbeernaert.restaurantpoc;

public class MenuItem {

    public String name;
    public String id;
    public String type;
    boolean checked;

    MenuItem(String name, String type, boolean checked, String id) {
        this.name = name;
        this.type = type;
        this.id = id;
        this.checked = checked;

    }
}
