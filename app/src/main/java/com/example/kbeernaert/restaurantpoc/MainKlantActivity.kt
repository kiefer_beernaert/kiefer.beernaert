package com.example.kbeernaert.restaurantpoc

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.DatabaseError
import android.view.Gravity
import android.text.Html
import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.DatabaseReference
import kotlinx.android.synthetic.main.activity_main_klant.*
import android.R.attr.checked
import android.content.Context
import android.content.Intent
import android.support.v4.content.res.TypedArrayUtils
import android.view.View
import android.widget.*
import com.example.kbeernaert.restaurantpoc.R.id.listView
import android.widget.Toast
import java.util.*


class MainKlantActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_klant)
        var listView = findViewById<ListView>(R.id.listView)
        var datamodels: ArrayList<MenuItem> = ArrayList()
        var dataModelsClickedZaal: ArrayList<MenuItem> = ArrayList()
        var dataModelsClickedKeuken: ArrayList<MenuItem> = ArrayList()
        var mDatabase = FirebaseDatabase.getInstance().getReference();
        var adapter: CustomAdapter
        adapter = CustomAdapter(datamodels, getApplicationContext())
        val drinksDB = mDatabase.child("drinks")
        val dessertDB = mDatabase.child("dessert")
        val mainCourseDB = mDatabase.child("mainCourse")
        val buttonConfirm = findViewById<Button>(R.id.buttonConfirm)
        val buttonCancel = findViewById<Button>(R.id.buttonCancel)
        buttonConfirm.setAlpha(.5f);
        buttonCancel.setAlpha(.5f);
        buttonCancel.setEnabled(false)
        buttonConfirm.setEnabled(false)

        drinksDB.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (i in dataSnapshot.children) {
                    Log.d("Tag", i.toString())
                    datamodels.add(MenuItem(i.child("name").value.toString(), "zaal", false, i.key))
                }
                adapter = CustomAdapter(datamodels, getApplicationContext())
                listView.setAdapter(adapter);
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }

        })

        mainCourseDB.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (i in dataSnapshot.children) {
                    datamodels.add(MenuItem(i.child("name").value.toString(), "keuken", false, i.key))
                }
                adapter = CustomAdapter(datamodels, getApplicationContext())
                listView.setAdapter(adapter);
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }

        })

        dessertDB.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (i in dataSnapshot.children) {
                    datamodels.add(MenuItem(i.child("name").value.toString(), "keuken", false, i.key))
                }
                adapter = CustomAdapter(datamodels, getApplicationContext())
                listView.setAdapter(adapter);
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }

        })

        listView.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                Log.d("tag", "clicked")
                val dataModel = datamodels.get(position)
                var found: Boolean = false
                dataModel.checked = !dataModel.checked
                Log.d("tag", dataModel.name + ' ' + dataModel.checked)
                if(dataModel.checked) {
                    if(dataModel.type === "keuken") {
                        dataModelsClickedKeuken.add(dataModel)
                    }
                    if(dataModel.type === "zaal") {
                        dataModelsClickedZaal.add(dataModel)
                    }
                } else {
                    if(dataModel.type === "zaal") {
                        for(i in dataModelsClickedZaal) {
                            if(i.name === dataModel.name) {
                                dataModelsClickedZaal.remove(i)
                                break
                            }
                        }
                    } else {
                        for(i in dataModelsClickedKeuken) {
                            if(i.name === dataModel.name) {
                                dataModelsClickedKeuken.remove(i)
                                break
                            }
                        }
                    }

                }
                if(dataModelsClickedKeuken.size > 0 || dataModelsClickedZaal.size > 0) {
                    Log.d("is not empty", dataModelsClickedKeuken.toString())
                    Log.d("is not empty", dataModelsClickedZaal.toString())
                    buttonCancel.setAlpha(1f);
                    buttonCancel.setEnabled(true)
                    buttonConfirm.setAlpha(1f);
                    buttonConfirm.setEnabled(true)
                } else {
                    Log.d("is empty", dataModelsClickedKeuken.toString())
                    Log.d("is empty", dataModelsClickedZaal.toString())
                    buttonCancel.setAlpha(.5f);
                    buttonCancel.setEnabled(false)
                    buttonConfirm.setAlpha(.5f);
                    buttonConfirm.setEnabled(false)
                }
                Log.d("KEUKEN", dataModelsClickedKeuken.toString())
                Log.d("ZAAL", dataModelsClickedZaal.toString())
                adapter.notifyDataSetChanged()

            }
        }

        buttonConfirm.setOnClickListener(View.OnClickListener {
            for( i in dataModelsClickedKeuken) {
                mDatabase.child("ordersKeuken").child(UUID.randomUUID().toString()).setValue(i)
            }
            for(i in dataModelsClickedZaal) {
                mDatabase.child("ordersZaal").child(UUID.randomUUID().toString()).setValue(i)
            }

            val context: Context = getApplicationContext();
            val text: CharSequence = "Uw bestelling is met success verzonden.";
            val duration = Toast.LENGTH_SHORT;

            val toast: Toast = Toast.makeText(context, text, duration);
            toast.show()

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        })

        buttonCancel.setOnClickListener(View.OnClickListener {
            dataModelsClickedKeuken = ArrayList()
            dataModelsClickedZaal = ArrayList()
            Log.d("datakeuken", dataModelsClickedKeuken.toString())
            Log.d("dataZaal", dataModelsClickedZaal.toString())
            adapter.notifyDataSetChanged()

            val context: Context = getApplicationContext();
            val text: CharSequence = "Uw bestelling is geannuleerd.";
            val duration = Toast.LENGTH_SHORT;

            val toast: Toast = Toast.makeText(context, text, duration);
            toast.show()

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)

        })



    }
}
